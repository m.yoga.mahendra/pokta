;====================================================================
; Processor		: ATmega8515
; Compiler		: AVRASM
;====================================================================

;====================================================================
; DEFINITIONS
;====================================================================

.include "m8515def.inc"
.def temp = r16 ; temporary register
.def led_data = r17
.def led_counter = r20
.def int_counter = r21
.def word_counter = r22
.def EW = r23 ; for PORTA
.def PB = r24 ; for PORTB
.def A  = r25

;====================================================================
; RESET and INTERRUPT VECTORS
;====================================================================

.org $00
rjmp MAIN
.org $01
rjmp ext_int0

;====================================================================
; CODE SEGMENT
;====================================================================

MAIN:

INIT_INTERRUPT:
	ldi temp,0b00000010
	out MCUCR,temp
	ldi temp,0b01000000
	out GICR,temp
	ldi led_counter,1
	ldi int_counter,1
	sei

INIT_STACK:
	ldi temp, low(RAMEND)
	ldi temp, high(RAMEND)
	out SPH, temp

INIT_LED:
	ser temp ; load $FF to temp
	out DDRC,temp ; Set PORTA to output

rcall INIT_LCD_MAIN

EXIT:
	rjmp EXIT


INPUT_TEXT:
	ldi ZH,high(2*message) ; Load high part of byte address into ZH
	ldi ZL,low(2*message) ; Load low part of byte address into ZL
	ret

INIT_LCD_MAIN:
	rcall INIT_LCD
	ldi word_counter, 0
	ser temp
	out DDRA,temp ; Set port A as output
	out DDRB,temp ; Set port B as output
	rcall INPUT_TEXT

LOADBYTE_UP:
	lpm ; Load byte from program memory into r0
	cpi word_counter,8		; check whether the string "POK " is written or not on the LCD
	breq CURSOR_DOWN	; if so, move cursor down
	mov A, r0 ; Put the character onto Port B
	rcall WRITE_TEXT
	adiw ZL,1 ; Increase Z registers
	inc word_counter
	rjmp LOADBYTE_UP

LOADBYTE_DOWN:
	lpm ; Load byte from program memory into r0
	tst r0 ; Check if we've reached the end of the message
	breq END_LCD ; If so, quit
	mov A, r0 ; Put the character onto Port B
	rcall WRITE_TEXT
	adiw ZL,1 ; Increase Z registers
	rjmp LOADBYTE_DOWN

END_LCD:
	rjmp LED_MAIN_LOOP

INIT_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	cbi PORTA,1 ; CLR RS
	ldi PB,$0E ; MOV DATA,0x0E --> disp ON, cursor ON, blink OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	rcall CLEAR_LCD ; CLEAR LCD
	cbi PORTA,1 ; CLR RS
	ldi PB,$06 ; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

CLEAR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,$01 ; MOV DATA,0x01
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall DELAY_01
	ret

WRITE_TEXT:
	sbi PORTA,1 ; SETB RS
	out PORTB, A
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	ret

CURSOR_DOWN:
	cbi PORTA,1 ; SETB EN
	ldi PB,$C0
	out PORTB,PB; Move cursor down
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rjmp CHECK_LED

ext_int0:
	mov led_counter,int_counter
	inc int_counter
	reti

CHECK_LED:
	cpi int_counter,1
	breq WRITE_1
	cpi int_counter,2
	breq WRITE_2
	cpi int_counter,3
	breq WRITE_3
	cpi int_counter,4
	breq RESET_COUNTER

RESET_COUNTER:
	ldi int_counter,1
	rjmp CHECK_LED

WRITE_1:
	ldi A,0x31
	rcall WRITE_TEXT
	rjmp LOADBYTE_DOWN

WRITE_2:
	ldi A,0x32
	rcall WRITE_TEXT
	rjmp LOADBYTE_DOWN

WRITE_3:
	ldi A,0x34
	rcall WRITE_TEXT
	rjmp LOADBYTE_DOWN

;====================================================================
; LED
;====================================================================
LED_MAIN_LOOP:
	cp led_counter, int_counter
	brne RETURN
	cpi int_counter,1
	breq LED_LOOP_1
	cpi int_counter,2
	breq LED_LOOP_2
	cpi int_counter,3
	breq LED_LOOP_4

RETURN:	
	mov led_counter, int_counter
	cpi led_counter,4
	breq RETURN_2
	rjmp INIT_STACK

RETURN_2:
	ldi led_counter,1
	rjmp INIT_STACK

LED_LOOP_1:
	;ldi led_data,0x00
	;out PORTC,led_data ; Update LEDS
	;rcall DELAY_01
	ldi led_data,0x01
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	rjmp LED_MAIN_LOOP

LED_LOOP_2:
	ldi led_data,0b00010001
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02	
	ldi led_data,0b00010001
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	lsl led_data
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	rjmp LED_MAIN_LOOP	
	
LED_LOOP_4:
	ldi led_data,0b01010101
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	ldi led_data,0b10101010
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	ldi led_data,0b01010101
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	ldi led_data,0b10101010
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	ldi led_data,0b01010101
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	ldi led_data,0b10101010
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	ldi led_data,0b01010101
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	ldi led_data,0b10101010
	out PORTC,led_data ; Update LEDS
	rcall DELAY_02
	rjmp LED_MAIN_LOOP

;====================================================================
; DELAYS
;====================================================================

DELAY_00:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; Delay 4 000 cycles
	; 500us at 8.0 MHz

	    ldi  r18, 6
	    ldi  r19, 49
	L0: dec  r19
	    brne L0
	    dec  r18
	    brne L0
	ret

DELAY_01:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; DELAY_CONTROL 40 000 cycles
	; 5ms at 8.0 MHz

	    ldi  r18, 52
	    ldi  r19, 242
	L1: dec  r19
	    brne L1
	    dec  r18
	    brne L1
	    nop
	ret

DELAY_02:
; Generated by delay loop calculator
; at http://www.bretmulvey.com/avrdelay.html
;
; Delay 160 000 cycles
; 20ms at 8.0 MHz

	    ldi  r18, 208
	    ldi  r19, 202
	L2: dec  r19
	    brne L2
	    dec  r18
	    brne L2
	    nop
		ret

;====================================================================
; DATA
;====================================================================

message:
.db "POK EZPZ"
.db " LED(s)"
