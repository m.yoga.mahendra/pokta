# POK Kelas C
## Kelompok : 10
#### Anggota:
Christian Denata

Muhamad Yoga Mahendra

Nicholas Pangestu
 
### Judul Aplikasi Proyek Akhir
Kalkulator Binary-Decimal-Octal-Hexadecimal (Addition, Substraction, Multiplication, Division)
 
### Deskripsi Proyek
Melakukan perhitungan aritmatika dalam 4 basis berbeda, yaitu Biner, Octal, Desimal, dan Heksadesimal.
Input akan diterima melalui keypad (angkanya sesuai tipe, tipenya boleh berbeda antar operand) dan button(jenis operasi aritmatika) dan output akan ditampilkan melalui lcd(sesuai tipe output yang diinginkan). 
 
### Fungsi dan Manfaat
Memudahkan programmer dalam melakukan kalkulasi aritmatika sederhana antarbasis berbeda, dan basis yang sama.
 
### Perkiraan tipe AVR yang digunakan
atmega8515 Dengan HAPSIM.
 
### Perangkat Input/Output
LCD : Layar Interaktif untuk meminta input dari user dan memberikan output kepada user

LED : Menyala jika input yang dimasukkan pengguna tidak bisa dilakukan/salah(contoh: zero division error).

keypad : Menerima input angka dari user, dan menerima input tipe tiap operand.

button : Memilih jenis operasi yang ingin dilakukan user
 
### Fitur :
Interrupt dan internalInterrupt
 
### Flow :
1. LCD menampilkan homepage yang berisi permintaan untuk memasukkan input.
2. User memasukkan operand yang ingin dioperasikan, tipenya, lalu memilih operasi yang ingin dilakukan, dan memasukkan tipe output yang diinginkan.
3. Jika ada error tertentu, LED akan menyala, jumlah yang menyala menentukan tipe error(masih didiskusikan)
4. Program menghitung kalkulasi aritmetikanya
5. Program mengirim hasil yang sudah dikonversi ke tipe output yang diinginkan ke LCD.
6. Meminta input user lagi, atau menghentikan program jika user mengetik exit.

### Petunjuk Penggunaan:
1. Tekan Tipe Angka yang ingin dihitung. (DEC, BIN, HEX)
2. Masukkan Angka pertama (maks 255 untuk DEC, 11111111 untuk BIN, FF untuk HEX)
3. Pilih operasi yang ingin dilakukan ("+", "-", "x", "/")
4. Masukkan Angka Kedua (maks 255 untuk DEC, 11111111 untuk BIN, FF untuk HEX)
5. Tekan "ENTER" untuk mendapatkan hasil operasi.

