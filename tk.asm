;====================================================================
; Processor		: ATmega8515
; Compiler		: AVRASM
;====================================================================

;====================================================================
; DEFINITIONS														 
;====================================================================
.include "m8515def.inc"
.DEF rmp = R16 ; define a multipurpose register
.DEF temp = R17
.def EW = r23 ; for PORTA
.def PB = r24 ; for PORTB
.def A  = r25
.def temp1= R18
.def temp1Hi=r14
.def temp2= R19
.def temp2Hi= R15
.def counter1=R12
.def counter2=R13
.def hasil=R21
.def carry= R10
.def Hi1=R8
.def Hi2=R9
.def min=R22
;r
; Keypad
.EQU pKeyOut = PORTD ; Output and Pull-Up-Port
.EQU pKeyInp = PIND  ; read keypad input
.EQU pKeyDdr = DDRD  ; data direction register of the port

;====================================================================
; RESET and INTERRUPT VECTORS
;====================================================================

.org $00
rjmp Main
.org $01
rjmp ext_int0
;====================================================================
; CODE SEGMENT
;====================================================================
; Init-routine

Main:
	ldi r20, 0
	mov r20,hi2
	mov r20, hi1
	ldi temp,10
	mov carry,temp
	ldi temp1,0
Stack:
	ldi temp, low(RAMEND)
	out SPL,temp
	ldi temp, high(RAMEND)
	out SPH, temp

InitLcd:
	cbi PORTA,1 ; CLR RS
	ldi PB,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall Delay
	cbi PORTA,1 ; CLR RS
	ldi PB,$0E ; MOV DATA,0x0E --> disp ON, cursor ON, blink OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall Delay
	cbi PORTA,1 ; CLR RS
	ldi PB,$06 ; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall Delay
	ser temp
	out DDRA,temp ; Set port A as output
	out DDRB,temp ; Set port B as output

InitKey:
	ldi rmp,0b11110000 ; data direction register column lines output
	out pKeyDdr,rmp    ; set direction register
	ldi rmp,0b00001111 ; Pull-Up-Resistors to lower four port pins
	out pKeyOut,rmp    ; to output port

InitInterrupt:
	ldi temp,0b00000010
	out MCUCR,temp
	ldi temp,0b01000000
	out GICR,temp
	sei	
	rcall ClearLcd ; CLEAR LCD
; Check any key pressed
AnyKey:
	ldi rmp,0b00001111 ; PB4..PB7=Null, pull-Up-resistors to input lines
	out pKeyOut,rmp    ; of port pins PB0..PB3
	in rmp,pKeyInp     ; read key results
	ori rmp,0b11110000 ; mask all upper bits with a one
	cpi rmp,0b11111111 ; all bits = One?
	breq NoKey         ; yes, no key is pressed



; Identify the key pressed
ReadKey:
	clc
	ldi ZH,HIGH(2*KeyTable) ; Z is pointer to key code table
	ldi ZL,LOW(2*KeyTable)

	; read column 1
	ldi rmp,0b10111111 ; PB6 = 0
	out pKeyOut,rmp
	in rmp,pKeyInp ; read input line
	ori rmp,0b11110000 ; mask upper bits
	cpi rmp,0b11111111 ; a key in this column pressed?
	brne KeyRowFound ; key found
	adiw ZL,4 ; column not found, point Z one row down

	; read column 2
	ldi rmp,0b11011111 ; PB5 = 0
	out pKeyOut,rmp
	in rmp,pKeyInp ; read again input line
	ori rmp,0b11110000 ; mask upper bits
	cpi rmp,0b11111111 ; a key in this column?
	brne KeyRowFound ; column found
	adiw ZL,4 ; column not found, another four keys down

	; read column 3
	ldi rmp,0b11101111 ; PB4 = 0
	out pKeyOut,rmp
	in rmp,pKeyInp ; read again input line
	ori rmp,0b11110000 ; mask upper bits
	cpi rmp,0b11111111 ; a key in this column?
	brne KeyRowFound ; column found
	adiw ZL,4 ; column not found, another four keys down
	
	; read column 4
	ldi rmp,0b01111111 ; PB7 = 0
	out pKeyOut,rmp
	in rmp,pKeyInp ; read last line
	ori rmp,0b11110000 ; mask upper bits
	cpi rmp,0b11111111 ; a key in this column?
	breq NoKey ; unexpected: no key in this column pressed
	

KeyRowFound: ; column identified, now identify row
	lsr rmp ; shift a logic 0 in left, bit 0 to carry
	brcc KeyFound ; a zero rolled out, key is found
	adiw ZL,1 ; point to next key code of that column
	rjmp KeyRowFound ; repeat shift

KeyFound: ; pressed key is found 
;	CLR TCNT0
	ldi temp,4
	lpm ; read key code to R0
	mov A, r0 ; Put the character onto Port B
	;cpi A, 0x0A
	;breq ClearLcd
	cpi A,0x0B
	breq TekanEnter
	cpi A,0x30
	brlt CekOperator
	cpi A,0x78
	breq TekanKali
	cp temp,counter1
	breq NoKey
	inc counter1
	rcall GantiLCD

CekDigit:
	subi A,48
	mov r11,A
	mul temp1,carry
	mov temp1,r0
	add temp1,r11
	ldi temp,2
	brlt Simpan
	rjmp NoKey

NoKey:
	rcall Delay
	rjmp AnyKey ; no key pressed

Simpan:
	mov temp1Hi,r11
	rjmp NoKey

Ganti:
	sub temp1,temp1Hi
	add temp1,r11
	ret

GantiLCD:
	sbi PORTA,1 ; SETB RS
	out PORTB, A
	cbi PORTA,0 ; CLR EN
	sbi PORTA,0 ; SETB EN
	ret

CekOperator:
	rcall GantiLCD
	mov hasil,r0
	rjmp TukarNilai
	
TukarNilai:
	mov temp2,temp1
	mov counter2,counter1
	clr temp1
	clr counter1
	rjmp NoKey


TekanKali:
	rcall GantiLCD
	ldi hasil,0x78
	rjmp TukarNilai

TekanEnter:

	cbi PORTA,1 ; SETB EN
	ldi PB,$C0
	out PORTB,PB; Move cursor down
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	ldi temp,0x30
	;out PORTB,temp
	;cbi PORTA,0 ; CLR EN
	;sbi PORTA,0 ; SETB EN
	;rjmp NoKey
	;cpi hasil,0x2B
	;breq Tambah
	;cpi hasil,0X2D
	;breq Kurang
	;cpi hasil,0x78
	;breq Kali
	;rjmp Bagi

Bagi:
	
Tambah:
	add temp1,temp2
	mov hasil,temp1
	cpi hasil,10
	brge AdaCarry1
	add temp1Hi,temp2Hi
	mov counter2,r0

AdaCarry1:
	subi hasil,10
	add temp1Hi,temp2Hi
	mov counter2,r0
	inc counter2
	
AdaCarry2:
Kurang:
	cp temp1,temp2
	brlt Minus
	sub temp1,temp2
	mov hasil,temp1
Minus:
	;sub temp2,temp1
	;mov hasil,temp2
	ldi min,1



Kali:
	mul temp1,temp2
	mov hasil ,r0
	mul temp1,temp2Hi


ClearLcd:
	;CLR TCNT0
	cbi PORTA,1 ; CLR RS
	ldi PB,$01 ; MOV DATA,0x01
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	ldi temp1,0
	ldi temp2,0
	mov counter1,temp1
	mov counter2, temp2
	rjmp NoKey
	rcall Delay
	;ret

Delay:
	; Generated by delay loop calculator
	; at http://www.bretmulvey.com/avrdelay.html
	;
	; Delay 20 000 cycles
	; 5ms at 4 MHz
	
	    ldi  temp, 150
		ldi  rmp, 249
 	L1: dec  temp
	    brne L1
	    dec rmp
    	brne L1
	ret

BeforeForever:
	inc r20
	;sbi PORTA,1 ; SETB RS
	;ldi A, 0x31
	;out PORTB, A
	;sbi PORTA,0 ; SETB EN
	;cbi PORTA,0 ; CLR EN
	ldi temp,0b00000010
	out MCUCR,temp
	ldi temp,0b01000000
	out GICR,temp
	sei

forever:
	rjmp NoKey
	;rjmp forever

ext_int0:
	cpi r20, 0
	breq BeforeForever
	dec r20
	rjmp AnyKey

; Table for code conversion
KeyTable:
.DB 0x31,0x34,0x37,0x0A ; First column, keys 1, 4, 7, and Clear
.DB 0x32,0x35,0x38,0x30 ; second column, keys 2, 5, 8, and 0
.DB 0x33,0x36,0x39,0x0B ; third column, keys 3, 6, 9, and Enter
.DB 0x2B,0x2D,0x78,0x2F ; fourth column, keys +, -, x, and /
